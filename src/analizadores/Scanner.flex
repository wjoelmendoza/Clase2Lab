
package analizadores;
import java_cup.runtime.Symbol;

%%
%cupsym Sym
%cup

%class Lexer
%public
%line
%char
%column
%full

digit   = [0-9]
number  = {digit}+
letter  = [a-zA-Z]
identifier = {letter}+

%%
"-"                {return new Symbol(Sym.minus, yyline, yycolumn, yytext());}
"/"                {return new Symbol(Sym.div, yyline, yycolumn, yytext());}
","                {return new Symbol(Sym.coma, yyline, yycolumn, yytext());}
";"                {return new Symbol(Sym.pyc, yyline, yycolumn, yytext());}
"("                {return new Symbol(Sym.lp, yyline, yycolumn, yytext());}
")"                {return new Symbol(Sym.rp, yyline, yycolumn, yytext());}
"="                 {return new Symbol(Sym.eq, yyline, yycolumn, yytext());}
"Dec"              {return new Symbol(Sym.dec, yyline, yycolumn, yytext());}
"Print"             {return new Symbol(Sym.print, yyline, yycolumn, yytext());}
{number}            {return new Symbol(Sym.num, yyline, yycolumn, yytext());}
{identifier}        {return new Symbol(Sym.id, yyline, yycolumn, yytext());}
[ \t\n]             {/*ignorar*/}
.               {System.err.println("No se reconoce: " + yytext());}