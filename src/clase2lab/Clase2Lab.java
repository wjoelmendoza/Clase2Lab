/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package clase2lab;

import analizadores.Lexer;
import analizadores.Parser;
import java.io.BufferedReader;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author wxjoy
 */
public class Clase2Lab {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        HashMap<String,Float> tabla =  new HashMap();
        String entrada = "";
        System.out.println("Ingrese operacion o 'end'para terminar");
        System.out.print("->");
        Scanner scaner = new Scanner(System.in);
        entrada = scaner.nextLine();
        
        while(!entrada.equalsIgnoreCase("end"))
        {
            Lexer lex = new Lexer( new BufferedReader(new StringReader(entrada)));
            Parser parse = new Parser(lex);
            parse.setTabla(tabla);
            try {
                parse.parse();
            } catch (Exception ex) {
                Logger.getLogger(Clase2Lab.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.print("->");
            entrada = scaner.nextLine();
        }
        
    }
    
}
